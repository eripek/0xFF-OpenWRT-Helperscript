# 0xFF-LEDE-Firmware-Helper
This repository contains a simple bash script and a set of packagelists, that will help you to create a highly customized firmware for your OpenWRT/LEDE device.The script will make use of the ImageBuilder for the ar71xx platform.

I've successfully created firmwares for TP-Link TL-WR1043NDv1 (caveats: memory low, brick possible due to low memory during the flashing procedure; advise: get stock firmware with an actual version of the bootloader, so you can recover from a bad flash by using the TFTP-without-serial-method), TL-WR1043NDv2, TL-WR842NDv1, TL-WDR3600. 

The script assemble-firmware.sh accepts parameters for a certain device target, a base image and a set of packages, each for a certain usecase. It will merge all these packagelists and remove duplicates from the main list. It will also accecpt a directory name as another parameter. In that directory we will store files to be overwritten in the produced image. The contents could be additional scripts, banners, config files, ssh-keys or precompiled custom packages. Remeber to resemble the original absolute path with these relative paths.

Installation:
1. Get ImageBuilder for ar71xx:
https://downloads.lede-project.org/releases/17.01.4/targets/ar71xx/generic/lede-imagebuilder-17.01.4-ar71xx-generic.Linux-x86_64.tar.xz

2. Decompress it

3. Get this package from github
git clone https://github.com/eripek/0xFF-LEDE-Firmware-Helper.git

4. Copy the contents to your lede image builder directory (from step 2).

5. Create your directory containing the 'overrides'. If lede-imagebuilder resides in /usr/src/ create a directory /usr/src/lede-additional-files-myflavor there. "myflavour" is an example for whatever you may want to call it. The script currently accepts "0xff", "none", "default" and "myflavour" as valid parameters. If you need to change that, edit assemble-firmware.sh and expand the list in line 28.

6. Create your firmware with ./assemble-firmware.sh:
	Usage:

a.) Make sure your internet connection is up, so you can download the neccessary packages now.

b.) ./assemble-firmware.sh [ROUTER-TARGET] [BASE-IMAGE] [OVERRIDE-DIRECTORY] [USECASE1] [USECASE2] [USECASE3] [USECASE...]

c.) !!! Watch for ERROR MESSAGES now !!! If there are no error messages, proceedelse carefully read the error message and go back to step 5 or 6.

7. Go to your newly created Binaries' directory
	cd bin/targets/ar71xx/generic/

This script's initial development was sponsored by https://www.grundsoli.de