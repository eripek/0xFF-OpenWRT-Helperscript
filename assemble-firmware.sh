#!/bin/bash
PACKAGEFILES=""
show_help(){
	echo "usage: $0 [[clean|info|[TARGET - see $0 info] [ADDITIONAL FILES DIR] {PACKAGELISTS (short names)}]"
	echo "examples:"
	echo -e "\t$0 clean"
	echo -e "\t$0 info"
	echo -e "\t$0 myroutermodel-v1 base myoverridefiles lte openvpn"
	return 0
}
if [ $# -le 2 ]; then
	if [ $# -eq 1 ]; then
		case $1 in 
		info)
			echo -en "no target defined - valid target's are:\n$(make info | egrep '^([A-Za-z0-9\-])+(:)+' | cut -d ':' -f1; echo -en "\r\n")"
		;;
		clean) 
			make clean
		;;
		*)
			show_help
		esac
	else
			show_help
	fi
else
	if [ $# -gt 2 ]; then
		echo $2 | grep "0xff\|none\|default\|myflavour"
		if [ $? -eq 0 ]; then
			FILES="$2"
		fi
	fi 
	if [ $# -ge 3 ]; then
		for i in "$@"; do
			echo ${i} | grep "base\-mod\|base\-0xff\|3g\|lte\|openvpn\|0xff\|printer\|statistics\|usbwlan\|zram"
			if [ $? -eq 0 ]; then
				PACKAGEFILES="${PACKAGEFILES} `echo packages-lede-17-${i}.txt`"
			fi
		done
		echo ${PACKAGEFILES} | grep -i base >/dev/null || PACKAGEFILES="$PACKAGEFILES `echo packages-lede-17-base-mod.txt`"
	else
		echo "no files given as parameter"
		exit 1	
	fi
	if [ ${#PACKAGEFILES[@]} -gt 0 ]; then
		INCLUDEPACKAGES=`cat $PACKAGEFILES | uniq | sort | tr "\n" " "`
		make clean
		make image PROFILE="$1" PACKAGES="`echo ${INCLUDEPACKAGES}`" FILES="../lede-additional-files-${FILES}/"
	fi
fi 
